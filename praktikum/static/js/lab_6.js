// Calculator 
$(document).ready(function(){ 
    var themes = [ 
        {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"}, 
        {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"}, 
        {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"}, 
        {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"}, 
        {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"}, 
        {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"}, 
        {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"}, 
        {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"}, 
        {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"}, 
        {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"}, 
        {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"} 
    ] 

    localStorage.setItem('themes', JSON.stringify(themes)); 

    //initialize mySelect 
    mySelect = $('.my-select').select2() 

    //put themes JSON to mySelect with key of 'data' 
    $('.my-select').select2({ 
        'data': JSON.parse(localStorage.getItem('themes')) 
    }); 

    var arr_of_themes = JSON.parse(localStorage.getItem('themes')); 
    var pinkTheme = arr_of_themes[1]; 
    var selectedTheme = pinkTheme; 

    //if there's already a selectedTheme, load it 
    if (localStorage.getItem("selectedTheme") !== null) { 
        selectedTheme = JSON.parse(localStorage.getItem("selectedTheme")); 
    } 

    //change bgcolor and fontcolor 
    $('body').css( 
        { 
            "background-color": selectedTheme.bcgColor, 
            "font-color": selectedTheme.fontColor 
        } 
    ); 

    //function for the button 
    $('.apply-button').on('click', function () { 
        var themeChosen = mySelect.val(); 
        //if the themeChosen id is valid 
        if (themeChosen < arr_of_themes.length) { 
            selectedTheme = arr_of_themes[themeChosen]; 
        } 

        //change bgcolor and fontcolor 
        $('body').css( 
            { 
                "background-color": selectedTheme.bcgColor, 
                "font-color": selectedTheme.fontColor 
            } 
        ); 

        //save or overwrite selectedTheme in localStorage 
        localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme)); 
    }) 

    // Chat 
      var kelas = "msg-send"; 
      $('.chat-text textarea').keypress(function(e) { 
        if (e.keyCode == 13 && !e.shiftKey) { 

          e.preventDefault(); // Prevent move to the new line when hit ENTER 

          var newMessage = $('textarea').val(); 
          var oldMessage = $('.msg-insert').html(); 

          $('.msg-insert').html(oldMessage + "<p class = '" + kelas + "'>" + newMessage + "</p>") 

          if (kelas == "msg-send") { 
            kelas = "msg-receive"; 
          } else { 
             kelas = "msg-send"; 
          } 

          $('textarea').val(""); // Empty the text area again 

        } 

      }); 

      // Close/Open the chat box 
      $('img').bind("click", function () { 
        $(".chat-body").toggle("slow"); 
        if(!hidden) { 
          hidden = true; 
          $("img").attr('src', up); 
        } else { 
          $("img").attr('src', down); 
          hidden = false; 
        } 
      }) 

}); 

var print = document.getElementById('print'); 
var erase = false; 

var go = function go(x) { 
  if (x === 'ac') { 
    print.value = ""; 
  } 
  else if(x === 'sin'){ 
    print.value = Math.sin(print.value); 
  } 
  else if(x === 'tan'){ 
    print.value = Math.tan(print.value); 
  } 
  else if(x === 'log'){ 
    print.value = Math.log(print.value); 
  } 
  else if (x === 'eval') { 
    print.value = Math.round(evil(print.value) * 10000) / 10000; 
    erase = true; 
  } else { 
    print.value += x; 
  } 
  var themes = getElementById('.my-select').value; 
    console.log(themes); 
}; 

function evil(fn) { 
  return new Function('return ' + fn)(); 
} 
// END