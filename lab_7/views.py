from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'

    paginator = Paginator(mahasiswa_list, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        mahasiswalist = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        mahasiswalist = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        mahasiswalist = paginator.page(paginator.num_pages)

    response = {"author": "Vincentius Aditya S", "mahasiswa_list": mahasiswalist, "friend_list": friend_list}
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

def isNpmValid(npm):
    friends = Friend.objects.all()

    for friend in friends:
        if friend.npm == npm:
            return False

    return True

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        data = {'id' : friend_id}
        return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': not isNpmValid(npm)
    }
    return JsonResponse(data)
